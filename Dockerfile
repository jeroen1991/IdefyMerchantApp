FROM ruby:2.2.2
WORKDIR /merchant_test_app

# Copy the current directory into the container at /idefy
COPY . /merchant_test_app

# Install required packages
RUN apt-get update -qq && apt-get install -y \
  nodejs \
  && rm -rf /var/lib/apt/lists/*

# Install Bundler and Ruby gems
RUN gem install bundler
RUN bundle install
RUN rake db:migrate