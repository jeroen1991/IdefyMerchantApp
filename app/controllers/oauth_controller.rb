class OauthController < ApplicationController   

    def scope1
        session[:scope] = "1"
        set_client_credentials("1")
        initialize_oauth_flow("scope_1")
    end

    def scope2
        session[:scope] = "2"
        set_client_credentials("2")
        initialize_oauth_flow("scope_2")
    end

    def scope3
        session[:scope] = "3"
        set_client_credentials("3")
        initialize_oauth_flow("scope_3")
    end

    def scope4
        session[:scope] = "4"
        set_client_credentials("4")
        initialize_oauth_flow("scope_4")
    end

    def callback
        code = params[:code]
        if code.present?
            set_client_credentials(session[:scope])
            access_token = client().auth_code.get_token(code, redirect_uri:  "http://localhost:3001/callback")

            @full_name = request_resource access_token, 'full_name'
            @email = request_resource access_token, 'email'
            @gender = request_resource access_token, 'gender'
            @date_of_birth = request_resource access_token, 'date_of_birth'
            @nationality = request_resource access_token, 'nationality'
            @picture = request_resource access_token, 'picture'
            @phone_number = request_resource access_token, 'phone'
        end
    end

    private

    def client
        return OAuth2::Client.new(
            @client_id,
            @client_secret,
            site: "http://localhost:3232/oauth/authorize"
        )
    end

    def initialize_oauth_flow(scope)
        redirect_to client().auth_code.authorize_url(:redirect_uri => "http://localhost:3001/callback") + 
            "&scope=" + scope
    end

    def set_client_credentials(scope)
        case scope
            when "1"
                @client_id = "c685b9610da292a65fa077535f5cd72fb769d9f80dd8970a56ac8277c4d82c9d"
                @client_secret = "04a4ae4af952f8ad0ea2277163a2a7f05c54d34ddee5053c1a976b7101e57959"
            when "2"
                @client_id = "edb1c3b1341cb637542faa35c6368bab2bf3c1deb0bfa73662196f8f9b7f445e"
                @client_secret = "b1696a7c07674751546a061b702eeed863317a5f64672bf8fb6ef5f6fa63b9ed"
            when "3"
                @client_id = "9ab33b98979ff73a29f9377b73f1b799670a6a6b65f392a15795a8e1b8082a0b"
                @client_secret = "b9cc4f80c75c30c3646c60b16bfbf9731dc467b891bfe6da07cb4ad44ad32a42"
            when "4"
                @client_id = "17609fd9f6b0f36e615db129bdb6018822867bb366c877a82695cc6b28fc4d35"
                @client_secret = "1ad015f8acb9fa4036078419cde7cac8e93cc3ade5b3ba03cb90b84156b7f56b"
        end
    end

    def request_resource token, resource
        begin
            return token.get("/api/v1/user/#{resource}").body
        rescue
            return nil
        end
    end
end
