Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'pages#home'
  get "callback" => "oauth#callback"

  # Scopes
  get "scope1" => "oauth#scope1"
  get "scope2" => "oauth#scope2"
  get "scope3" => "oauth#scope3"
  get "scope4" => "oauth#scope4"
end
